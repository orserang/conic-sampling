#ifndef _ConicSampling_H
#define _ConicSampling_H

#include "LPSolver.h"

class ConicSampling : public LPSolver
{
public:
  ConicSampling();
  ConicSampling(char * fname);
  Vector solve();

  //  void loadArray(istream & fin);
  //  void load(istream & fin);

  void saveArray(ostream & fout);

  void checkFeasibility() const;
protected:
  void descentStep();
  
  void clear();

  // returns false if optimal
  bool randomStep();

  int advance(const Vector & p);
  void addConstraint(int index);
  void addConstraintLinearProjection(int index);
  void addConstraintGramSchmidt(int index);

  //  void normalizeLP();
  //  void addPositivityConstraints();

  Vector getProjection() const;

  Matrix getSpanningVectors() const;
  Matrix getOldSpanning();

  Matrix S;
  Matrix SSTransInverse;

  Numerical hitChecker;

  Set currentGliders;

  GramSchmidt gs;
};

#endif

