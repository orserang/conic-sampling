#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include "ConicSampling.h"

using namespace std;

int main(int argc, char**argv)
{
  if ( argc == 2 )
    {
      cout.precision(15);

      int seed;
      //      cin >> seed;
      seed = time(NULL);
      srand(seed);

      cout << seed << endl;
      ConicSampling fs(argv[1]);

      //      fs.outputMathematica("/tmp/lp.nb");

      fs.solve();

      fs.printResult();
    }
  else
    {
      cerr << "usage: ConicSampling-LPSolve <Sparse LP-File>" << endl << endl;
    }
  return 0;
}


