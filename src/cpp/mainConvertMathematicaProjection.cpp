#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include "ConicSamplingPolytopeProjection.h"

using namespace std;

int main(int argc, char**argv)
{
  if ( argc == 3 )
    {
      cout.precision(15);
      ConicSamplingPolytopeProjection primal(argv[1]);
      //      primal.solve();
      //      primal.printResult();
      primal.outputMathematica(argv[2]);
    }
  else
    {
      cerr << "usage: Projection2Mathematica <Projection filename> <Mathematica filename>" << endl << endl;
    }
  return 0;
}


