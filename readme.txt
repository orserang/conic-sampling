About
========================================================
Written by Oliver Serang 2012


This simple LP solving library illustrates

     the conic sampling method described in

     	 Serang O (2012) Conic Sampling: An Efficient Method for
     	 Solving Linear and Quadratic Programming by Randomly Linking
     	 Constraints within the Interior. PLoS ONE 7(8):
     	 e43706. doi:10.1371/journal.pone.0043706

     the simplex method (with multiple pivot rules)

     the affine scaling method


Please see the paper for pseudocode and explanations of all methods.


There is also a small example for running projections on polytopes.


The vector library included in this code implements dual sparse-dense
vectors (i.e. they are stored in both formats for fastest inner
product).


Formatting
========================================================
LP text files are of the form:

n
xNames
x
f
b
A

where
	n is the dimension of the x vector

	xNames is an array of the x variable names
	
	x is the initial x value (it should be feasible-- if it is
	not, construct a two phase problem to first "climb" into the
	feasible region or use the "big-M" method as described in
	Serang 2012).

	and the LP is defined by
	    maximize x^T f : A x >= b (or A x \geq b in LaTeX notation)

Note that arrays must have spaces between the contents and the delimiters ',' or '{' or '}'

Dual sparse-dense vectors are written ( length { index1 , index2 } ; {
value1 , value2 } ) to indicate that all indices have zero value
except indices index1 and index2, which have respective values value1
and value2.

There are executables in bin/ that can generate and solve both LPs and
projections on polytopes. You can use these to generate simple
problems to get your bearings (and can compare the results in
Mathematica as described below).


A demo problem test.lp is included in this module. It is a two dimensional problem with three constraints:

0.1 x0 + 1 x1 >= 0
1 x0 + 0.1 x1 >= 0
0.1 x0 + 1 x1 >= 0

(in addition to the standard positivity constraints x0 >= 0 and x1 >=
0, which are added by the call addPositivityConstraints() in
ConicSampler.cpp).

this problem maximizes the function 1 x0 - 0.1 x1.

The optimum should be x0=1, x1=0

Serializing for Mathematica
========================================================
An LPSolver object can be serialized to Mathematica format using the following function:
void LPSolver::outputMathematica(char * fname) const

Furthermore, the executables LP2Mathematica and Projection2Mathematica
executables from the bin/ directory can convert the LP and projection
text files to Mathematica notebooks for solving the same problems.
